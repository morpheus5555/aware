# Stage 1 - Create cached node_modules
FROM arm32v7/node:10-buster as node_cache

WORKDIR /cache/

RUN apt-get update && apt-get install -y --no-install-recommends \
  build-essential python libssl-dev && \
  apt-get purge -y --auto-remove && rm -rf /var/lib/apt/lists/*

COPY package.json .
RUN npm install -s --no-progress

# Stage 2 - Build the Electron application
FROM arm32v7/node:10-buster

WORKDIR /aware 

RUN apt-get update && apt-get install -y --no-install-recommends \
  qtbase5-dev bsdtar build-essential autoconf libssl-dev lzip rpm unzip wget libc6 glibc-source \
  libsecret-1-dev \
  libxss1 libxtst6 libgtk-3-0 libgconf-2-4 libasound2 net-tools network-manager x11-xserver-utils \
  sox libsox-fmt-mp3 \
  libopenjp2-tools && \
  apt-get purge -y --auto-remove && rm -rf /var/lib/apt/lists/*

ENV LANG C.UTF-8
ENV LANGUAGE C.UTF-8
ENV LC_ALL C.UTF-8

ENV DEBUG_COLORS true
ENV FORCE_COLOR true

ENV ELECTRON_CACHE /root/.cache/electron
ENV ELECTRON_BUILDER_CACHE /root/.cache/electron
ENV NPM_CONFIG_PREFIX /root/.npm-global
ENV NODE_ENV production
ENV PATH $PATH:/root/.npm-global/bin

COPY --from=node_cache /cache/ .
COPY . .

RUN npm run build-pi

CMD ["./build/linux-armv7l-unpacked/aware"]
