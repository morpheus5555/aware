# aware

A BuildingLink LLC Sofware.

#### Build Setup

Due to the boilerplate used,
This software will have some bugs with the latest version of nodejs.

Using a stable version such as NodeJS 8 is recommended.

``` bash
# Install nodejs 8
npm install -g n   # Install n globally
n 8                # Install and use v8.x.x
```

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:9080
npm run dev

# build electron application for production
npm run build

# run unit & end-to-end tests
npm test


# lint all JS/Vue component files in `src/`
npm run lint

```

---
