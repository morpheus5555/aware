#!/usr/bin/env bash

# Stores all wifi connections in a variable
wifi="$(cd /etc/NetworkManager/system-connections && grep -r -n type=wifi)"

# Stores just the wifi name by picking up the content before the first :
wifi_name="$(cd /etc/NetworkManager/system-connections && cut -d ':' -f -1 <<< $wifi)"

echo "WIFI:"
echo $wifi_name

# Remove wifi names with space in name
cd /etc/NetworkManager/system-connections && find $wifi_name . -regex '.* .*' -delete

# Remove wifi names with no spaces in name
cd /etc/NetworkManager/system-connections && rm $wifi_name

# Get wifi connections using nmcli
wifi_list=`nmcli --fields UUID,TYPE c show | grep wifi | cut -d ' ' -f 1`

# Remove wifi connections using nmcli
nmcli c delete $wifi_list
