#!/bin/bash

export USER=root
id -u $USER > /dev/null
export PASSWD=$1
export ORIGPASS=`grep -w "$USER" /etc/shadow | cut -d: -f2`
export ALGO=`echo $ORIGPASS | cut -d'$' -f2`
export SALT=`echo $ORIGPASS | cut -d'$' -f3`
GENPASS=$(perl -le 'print crypt("$ENV{PASSWD}","\$$ENV{ALGO}\$$ENV{SALT}\$")')
if [ "$GENPASS" == "$ORIGPASS" ]
then
        echo "0"
        exit 0
else
        echo "1"
        exit 1
fi
