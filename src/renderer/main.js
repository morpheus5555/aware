import Vue from 'vue'
import axios from 'axios'
import { webFrame } from 'electron'

import App from './App'
import router from './router'
import store from './store'

import './assets/bulma-config.sass'
import 'animate.css/animate.min.css'

import cron from 'cron'
import redis from './redis'

if (!process.env.IS_WEB) {
  Vue.use(require('vue-electron'))
}

// Disable zooming
webFrame.setVisualZoomLevelLimits(1, 1)

Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

/* eslint-disable no-new */
const vue = new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')

vue.$store.commit('SHIFT_NOTIFICATION_BY_TYPE', '0')

const notificationChecker = new cron.CronJob('0/5 * * * * *', () => {
  redis.fetchNotifications(notifications => {
    redis.archiveNotifications(notifications)
    vue.$store.dispatch('addNotifications', notifications)
  })
}, null, true)
notificationChecker.start()

/* ipcMain.on('synchronous-message', (event, arg) => {
  if (arg === 'get_version') {
    client.get('version', (err, reply) => {
      if (err) {
        console.error(err)
      }
      event.returnValue = reply
    })
  } else if (arg === 'get_updated') {
    client.get('updated', (err, reply) => {
      if (err) {
        console.error(err)
      }
      event.returnValue = reply
    })
  }
}) */
