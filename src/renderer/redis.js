// Redis Client Service

// (Not Urgent) TODO: Replace callbacks by Promises,
// as Redis Node.js client does not handle them
// by default.

const isProd = process.env.NODE_ENV === 'production'

const redis = require('redis')
const client = redis.createClient({
  host: isProd ? 'redis' : '127.0.0.1'
})

// Set dummy data to version and updated if they're undefined
client.setnx('version', '4.7.2')
client.setnx('updated', Date.now())

client.on('error', function (error) {
  console.error(error)
})

const getValue = (key, callback) => {
  client.get(key, (err, reply) => {
    if (err) {
      console.error(err)
    }
    callback(reply)
  })
}

/* Transform fetched Redis cache string to
 * array and do a valid check (Can be improved in the future)
 */
const redisToArray = (data, n) =>
  data
    .map(element => element.split('|'))
    .filter(function isValid (element, index) {
      if (element.length !== n) {
        console.error(`Notification doesn't have ${n} arguments or is invalid.`)
      } else {
        return true
      }
    })

const fetchNotifications = callback =>
  client
    .multi()
    .lrange('notifications', '0', '-1', function (err, notifications) {
      if (err) {
        console.error(err)
      }

      if (notifications && notifications.length > 0) {
        notifications = redisToArray(notifications, 3)
      }
      callback(notifications)
    })
    .ltrim('notifications', '9999', '-1', function (err, data) {
      if (err) {
        console.error(err)
      }
      console.log(data)
    })
    .exec()

const fetchHistoricalNotifications = callback =>
  client.lrange('historicalNotifications', '0', '-1', function (err, notifications) {
    if (err) {
      console.error(err)
    }

    if (notifications && notifications.length > 0) {
      notifications = redisToArray(notifications, 3)
    }
    callback(notifications)
  })

const archiveNotifications = notifications =>
  notifications.map(notification => {
    client.lpush('historicalNotifications', notification.join('|'))
  })

export default {
  fetchNotifications,
  fetchHistoricalNotifications,
  archiveNotifications,
  getValue
}
