import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: require('@/components/LandingPage').default,
      meta: {
        noScroll: true
      }
    },
    {
      path: '/status',
      name: 'Status',
      component: require('@/components/Status').default
    },
    {
      path: '/weather',
      name: 'Weather',
      component: require('@/components/Weather').default
    },
    {
      path: '/aboutus',
      name: 'About Us',
      component: require('@/components/AboutUs').default
    },
    {
      path: '/annunciator',
      name: 'Annunciator',
      component: require('@/components/Annunciator').default
    },
    {
      path: '/settings',
      name: 'Settings',
      component: require('@/components/Settings').default,
      children: [
        {
          path: 'general',
          name: 'General Settings',
          component: require('@/components/General').default
        },
        {
          path: 'wifi',
          name: 'Wifi Settings',
          component: require('@/components/Wifi').default
        },
        {
          path: 'timezone',
          name: 'Time Zone',
          component: require('@/components/TimeZone').default
        },
        {
          path: 'admin',
          name: 'Admin Tools',
          component: require('@/components/AdminTools').default
        }
      ]
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
