import { exec } from 'child_process'
import si from 'systeminformation'
import playSound from 'play-sound'

const player = playSound()

// import say from 'say'

let emergencyStop = false

const state = {
  name: null,
  timezone: 'America/New_York',
  time: '00:00',
  notifications: [],
  historicalNotifications: []
}

const mutations = {
  UPDATE_NAME (state, name) {
    state.name = name
  },
  UPDATE_TIMEZONE (state, timezone) {
    state.timezone = timezone
  },
  UPDATE_TIME (state, time) {
    const value = new Date(time.current)
      .toLocaleString('en-US', {
        timeZone: state.timezone || 'America/New_York'
      }).slice(-11, -6)
    state.time = value
  },
  ADD_NOTIFICATION (state, notification) {
    state.notifications.push(notification)
  },
  ADD_HISTORICAL_NOTIFICATION (state, notification) {
    state.historicalNotifications.push(notification)
  },
  POP_NOTIFICATION (state) {
    state.notifications.shift()
  },
  DELETE_ALL_HISTORICAL_NOTIFICATIONS (state) {
    state.historicalNotifications = []
  },
  SHIFT_NOTIFICATION_BY_TYPE (state, type) {
    if (type === '1') emergencyStop = true

    state.notifications.forEach((notification, index, notifications) => {
      if (notification.type === type) {
        state.notifications.splice(index, 1)
      }
    })
  }
}

const actions = {
  updateName ({ commit }, name) {
    commit('UPDATE_NAME', name)
  },
  updateTimezone ({ commit }, timezone) {
    commit('UPDATE_TIMEZONE', timezone)
    this.dispatch('updateTime')
  },
  updateTime ({ commit }) {
    let time = si.time()
    commit('UPDATE_TIME', time)
  },
  addNotifications ({ commit }, notifications) {
    if (!notifications || notifications.length < 1) {
      return
    }

    const notifs = notifications.map(notification => {
      const n = {
        timestamp: notification[0],
        type: ['0', '1'].includes(notification[1]) ? notification[1] : '0',
        message: notification[2]
      }

      if (n.type === '0') {
        setTimeout(() => {
          commit('SHIFT_NOTIFICATION_BY_TYPE', '0')
        }, 11000)
      }

      commit('ADD_NOTIFICATION', n)
      return n
    })

    exec('xset -q | grep "Monitor is"', (err, stdout, stderr) => {
      if (err) { console.error(err) }

      const turnScreenOff = stdout.includes('Off')

      // Turn screen on and then off if it was asleep before
      exec('xset dpms force on', () => {
        if (turnScreenOff && notifs[notifs.length - 1].type === '0') {
          exec('xset dpms 10 10') // Set time interval to sleep to 10 sec
          setTimeout(() => exec('xset dpms 600 600'), 10000) // Reset time interval after 10 sec
        }
      })

      const sayAlert = () => {
        player.play('static/alert.mp3', err => {
          if (err) console.error(err)

          if (!emergencyStop) setTimeout(sayAlert, 400)
          else emergencyStop = false
        })
      }
      const sayMessage = () => {
        player.play('static/message.mp3', err => {
          if (err) console.error(err)
          console.log('end')
        })
      }

      if (notifs[notifs.length - 1].type === '1') sayAlert()
      else sayMessage()
    })
  },
  addHistoricalNotifications ({ commit }, notifications) {
    commit('DELETE_ALL_HISTORICAL_NOTIFICATIONS')
    notifications.map(notification => {
      const n = {
        timestamp: notification[0],
        type: ['0', '1'].includes(notification[1]) ? notification[1] : '0',
        message: notification[2]
      }
      commit('ADD_HISTORICAL_NOTIFICATION', n)
    })
  },
  archiveLastNotification ({ commit, state }) {
    if (!state.notifications || state.notifications.length < 1) {
      return
    }

    commit('POP_NOTIFICATION')
  }
}

export default {
  state,
  mutations,
  actions
}
