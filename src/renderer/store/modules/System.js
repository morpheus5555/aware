import si from 'systeminformation'

const state = {
  os: {},
  versions: {}
}

const mutations = {
  UPDATE_OS (state, os) {
    state.os = os
  },
  UPDATE_VERSIONS (state, versions) {
    state.versions = versions
  }
}

const actions = {
  updateOS ({ commit }) {
    si.osInfo(data => {
      commit('UPDATE_OS', data)
    })
  },
  updateVersions ({ commit }) {
    si.versions(data => {
      commit('UPDATE_VERSIONS', data)
    })
  }
}

export default {
  state,
  mutations,
  actions
}
