import wifi from 'node-wifi'
import si from 'systeminformation'

const state = {
  main: 0,
  quality: 0,
  type: null
}

const mutations = {
  DECREMENT_MAIN_COUNTER (state) {
    state.main--
  },
  INCREMENT_MAIN_COUNTER (state) {
    state.main++
  },
  UPDATE_QUALITY (state, quality) {
    state.quality = quality
  },
  UPDATE_TYPE (state, type) {
    state.type = type
  }
}

const actions = {
  updateQuality ({ commit }) {
    wifi.getCurrentConnections((err, currentConnections) => {
      if (err) {
        console.error(err)
      } else if (currentConnections.length !== 0) {
        commit('UPDATE_QUALITY', currentConnections[0].quality)
      } else {
        commit('UPDATE_QUALITY', -1)
      }
    })
  },
  updateType ({ commit }) {
    si.networkInterfaceDefault(defaultInterface => {
      const { spawn } = require('child_process')
      spawn('nmcli', ['d', 'show', defaultInterface]).stdout.on('data', (data) => {
        var uint8array = new TextEncoder('utf-8').encode(data)
        var output = new TextDecoder('utf-8').decode(uint8array)

        let outputIsValid = false
        let isWifi = output.split('\n').reduce((acc, line) => {
          if (line.includes('TYPE')) {
            outputIsValid = true
          }
          if (line.includes('TYPE') && (line.includes('wifi') || line.includes('ethernet'))) {
            acc = true
          }
          return acc
        }, false)

        if (outputIsValid) {
          if (isWifi === true) {
            commit('UPDATE_TYPE', 'wifi')
          } else {
            commit('UPDATE_TYPE', 'gsm')
          }
        }
      })
    })
  }
}

export default {
  state,
  mutations,
  actions
}
